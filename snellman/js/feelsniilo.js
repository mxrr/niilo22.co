var currentImg = null;

function sanonta(){

	$.get('quote', function(data){
            var array = data.split('\n');
			var i = Math.floor((Math.random() * array.length - 1) + 0);

			if(array[i] == null){
				sanonta();
			}
			else{
                unfade(document.getElementById("skinners"));
                //currentImg = null;
				document.getElementById("lause").innerHTML = array[i];
            }

            if(array[i] == "Tämmönen Skinnersin hasselmint"){
                currentImg = document.getElementById("skinners");
            }

        });
}

function refreshClick(){
	sanonta();
	document.getElementById("refreshB").style.background = "rgba(255, 255, 255, 0.7)";
}

function refreshReset(){
	document.getElementById("refreshB").style.background = "rgba(51, 51, 51, 0.7)";
}

function refreshHover(){
	document.getElementById("refreshB").style.borderColor = "rgba(255, 255, 255, 0.7)";
}

function refreshLeave(){
	document.getElementById("refreshB").style.borderColor = "darkslategrey";
}

function copyTextContent(){
	var copyText = document.getElementById("lause").innerHTML;
 	var dummy = $('<input>').val(copyText).appendTo('body').select()
 	document.execCommand('copy')

	setTimeout(dummy.remove(),4000);

      console.log(copyText);
      unfade(document.getElementById("copyIcon"));
      setTimeout(fade(document.getElementById("copyIcon")), 999999)
}

function textHover(){
    document.getElementById("lause").style.color = "rgba(255, 255, 255, 0.5)"
}

function textLeave(){
    document.getElementById("lause").style.color = "rgba(255, 255, 255, 1)"
}



function unfade(element) {
    var op = 0.1;
    element.style.display = 'block';
    var timer = setInterval(function () {
        if (op >= 1){
            clearInterval(timer);
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op += op * 0.1;
    }, 10);
}

function fade(element) {
    var op = 1;
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}
