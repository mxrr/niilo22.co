var currentImg = null;

function sanonta(){

	$.get('niilo', function(data){
      var array = data.split('\n');
			var i = Math.floor((Math.random() * array.length - 1) + 0);

			if(array[i] == null){
				sanonta();
			}
			else{
        if(currentImg != null){
						fade(currentImg);
            currentImg = null;
				}
				document.getElementById("lause").innerHTML = array[i];
				currentImg = showImg(array[i], currentImg);
				}
  });
}

function showImg(text, currentImg){
	if(text.includes("Tämmönen Skinnersin hasselmint")){
		currentImg = document.getElementById("skinners");
		unfade(currentImg);
		return currentImg;
	}
	else if(text.includes("Rutles!")){
		currentImg = document.getElementById("rutles");
		unfade(currentImg);
		return currentImg;
	}
	else if(text.includes("fitness spinni")){
		currentImg = document.getElementById("fitness");
		unfade(currentImg);
		return currentImg;
	}
	else{
		return null;
	}
}



function buttonClick(element){

	if(element.id == "refreshB"){
		sanonta();
	}
	else if(element.id == "rulettiB"){
		window.location = "http://www.niilo22.co/ruletti/";
	}
	else if(element.id == "snellmanB"){
		window.location = "http://www.niilo22.co/snellman/";
	}

	element.style.background = "rgba(255, 255, 255, 0.7)";
}

function buttonReset(element){
	element.style.background = "rgba(51, 51, 51, 0.7)";
}

function buttonHover(element){
	element.style.borderColor = "rgba(255, 255, 255, 0.7)";
}

function buttonLeave(element){
	element.style.borderColor = "darkslategrey";
}

function copyTextContent(){
	var copyText = document.getElementById("lause").innerHTML;
 	var dummy = $('<input>').val(copyText).appendTo('body').select()
 	document.execCommand('copy')

	setTimeout(dummy.remove(),4000);

      console.log(copyText);
      unfade(document.getElementById("copyIcon"));
      setTimeout(fade(document.getElementById("copyIcon")), 999999)
}

function textHover(){
    document.getElementById("lause").style.color = "rgba(255, 255, 255, 0.5)"
}

function textLeave(){
    document.getElementById("lause").style.color = "rgba(255, 255, 255, 1)"
}



function unfade(element) {
    var op = 0.1;
    element.style.display = 'block';
    var timer = setInterval(function () {
        if (op >= 1){
            clearInterval(timer);
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op += op * 0.1;
    }, 10);
}

function fade(element) {
    var op = 1;
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}
