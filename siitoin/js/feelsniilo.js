var currentImg = null;

function sanonta(){

	$.get('siitoin', function(data){
            var array = data.split('\n');
			var i = Math.floor((Math.random() * array.length - 1) + 0);

			if(array[i] == null){
				sanonta();
			}
			else{
                unfade(document.getElementById("skinners"));
                //currentImg = null;
				document.getElementById("lause").innerHTML = array[i];
            }

            if(array[i] == "Tämmönen Skinnersin hasselmint"){
                currentImg = document.getElementById("skinners");
            }

						audioCheck(array[i]);
        });
}

function audioCheck(text){
	//var text = document.getElementById("lause").innerHTML;

	if(text.includes("Me natsit, me jalostamme rotua")){
		playAudio("jalostamme");
	}
	else if(text.includes("90% ruotsalaisista ovat homoseksuaalia")){
		playAudio("homoseksuaalia");
	}
	else if(text.includes("näin on marjat")){
		playAudio("marjat");
	}
	else if(text.includes("meidän kermainen esi-isä")){
		playAudio("kermainen");
	}
	else if(text.includes("I am devil")){
		playAudio("devil");
	}
	else if(text.includes("homoloordi")){
		playAudio("homolordi");
	}
	else if(text.includes("Mä vedän turpaan tota saatanan loordia")){
		playAudio("turpaan");
	}
	else if(text.includes("Mun mielestä se on erittäin väärin että tänne tuodaan noita mutakuonoja")){
		playAudio("tuodaan");
	}
	else if(text.includes("Ne ei tee muuta kun raiskaa meidän naisia ja levittää aidsia")){
		playAudio("aidsia");
	}
	else if(text.includes("Olisin sitä mieltä että lähetetään kaikki pakolaisneekerit lappiin lumitöihin, joulupukkikin tarvitsee mustia tonttuja, se on tasa-arvoa")){
		playAudio("tasaarvoa");
	}
	else if(text.includes("saatana elää ja voi hyvin tässä maailmassa")){
		playAudio("saatana");
	}
	else if(text.includes("")){
		playAudio("");
	}
	else if(text.includes("")){
		playAudio("");
	}
	else if(text.includes("")){
		playAudio("");
	}
	else if(text.includes("")){
		playAudio("");
	}
}



function playAudio(filename){
	let audioMP3 = new Audio("snd/" + filename + ".mp3");
	audioMP3.play();
}

function refreshClick(){
	sanonta();
	document.getElementById("refreshB").style.background = "rgba(255, 255, 255, 0.7)";
}

function refreshReset(){
	document.getElementById("refreshB").style.background = "rgba(51, 51, 51, 0.7)";
}

function refreshHover(){
	document.getElementById("refreshB").style.borderColor = "rgba(255, 255, 255, 0.7)";
}

function refreshLeave(){
	document.getElementById("refreshB").style.borderColor = "darkslategrey";
}

function copyTextContent(){
	var copyText = document.getElementById("lause").innerHTML;
 	var dummy = $('<input>').val(copyText).appendTo('body').select()
 	document.execCommand('copy')

	setTimeout(dummy.remove(),4000);

      console.log(copyText);
      unfade(document.getElementById("copyIcon"));
      setTimeout(fade(document.getElementById("copyIcon")), 999999)
}

function textHover(){
    document.getElementById("lause").style.color = "rgba(255, 255, 255, 0.5)"
}

function textLeave(){
    document.getElementById("lause").style.color = "rgba(255, 255, 255, 1)"
}



function unfade(element) {
    var op = 0.1;
    element.style.display = 'block';
    var timer = setInterval(function () {
        if (op >= 1){
            clearInterval(timer);
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op += op * 0.1;
    }, 10);
}

function fade(element) {
    var op = 1;
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}
